# aplicacionweb

Aplicación web básica y de ejemplo siguiendo el patrón de arquitectura MVC (Modelo-Vista-Controlador) mediante el uso de
clases de Java (Modelos), JavaServer Pages (Vistas) y Java Servlets (Controladores).

## Requerimientos

Se requiere la instalación de:

-  [Docker](https://www.docker.com):
   para la gestión del _entorno local de ejecución_ de la aplicación mediante [Apache Tomcat](https://tomcat.apache.org)
   en un contenedor.
-  [GNU Make](https://www.gnu.org/software/make):
   para la programación y ejecución de tareas.
-  [Apache Maven](https://maven.apache.org):
   para la gestión del proyecto y sus dependencias.
-  [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli):
   para la gestión del _entorno producción de ejecucón_ de la aplicación mediante [Heroku](https://heroku.com).

## Preparación

Una vez clonado el proyecto y estando en su interior:

1. Descarga la imagen y crea el contenedor:

   ```shell
   $ make docker-image
   $ make docker-container
   ```

2. Valida la disponibilidad de dependencias y verifica la ejecución de pruebas:

   ```shell
   $ make mvn-validate
   $ make mvn-verify
   ```

3. Estando el contenedor en ejecución, empaqueta el proyecto en formato WAR y realiza el deployment del mismo
   al contenedor:

   ```shell
   $ make mvn-deploy
   ```

   Al terminar podrás acceder a [http://localhost:8080](http://localhost:8080).

4. Dando por hecho que ya existe la _aplicación de Heroku_ puedes hacer el despliegue a Heroku de la
   aplicación mediante:

   ```shell
   $ make heroku-deploy
   ```

   Al terminar podrás acceder a [https://torresvelascoalvaroeduardo.herokuapp.com/](https://torresvelascoalvaroeduardo.herokuapp.com/)

## Uso

Puedes editar o crear nuevos modelos, vistas y controladores tantos como te sean necesarios tomando en cuenta
la organización del proyecto:

-  Los Modelos (_clases de Java_) residen dentro de `src/main/java/programacionweb/modelos`.
-  Las Vistas (_JavaServer Pages_) residen dentro de `src/main/webapp/WEB-INF/vistas`.
-  Los Controladores (_Java Servlets_) residen dentro de `src/main/java/programacion/controladores`.

## Despliegue

-  Local:

   La tarea `make mvn-deploy` se encarga de hacer el despliegue de la aplicación al contenedor de Docker.

   La tarea _limpia_ el proyecto, lo _empaqueta_ como archivo `target/ROOT.war` y finalmente este
   archivo es copiado al contenedor.

   Por cada modificación local (crear-borrar-editar Modelos, Vistas o Controladores, etc) que haces del proyecto
   puedes hacer un despliegue local.

-  Producción:

   La tarea `make heroku-deploy` se encarga de hacer el despliegue de la aplicación a
   [Heroku](https://dashboard.heroku.com)

   La tarea envía el archivo `target/ROOT.war` a la _infraestructura_ de Heroku, donde será analizado, validado
   y verificado mediante Apache Maven.

   Una vez que de manera local hayas corroborado la correcta ejecución de la aplicación puedes hacer un
   despliegue a producción.


## Licencia

[MIT](https://choosealicense.com/licenses/mit/)
