<%-- -- Directiva **page**, establece lo siguiente: -- -- * language: lenguaje de programación a usar en este JSP: java
  -- * contenType: tipo de contenido que generará este JSP: text/html -- * pageEncoding: codificación de carácteres a
  usar para este JSP: UTF-8 --%>
  <%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>

    <%-- -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡
      IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -- -- JSP forza el inicio-creación de una sesión (JSESSIONID) con el -- cliente
      (navegador web) por lo que todo cliente creará una sesión. -- -- Si se quiere desactivar esta carácteristica se
      establece el -- atributo **session** a **false** en la directiva **page** para todos -- los JSP que uno use en el
      proyecto. -- -- <%@ page -- session="false" -- language="java" -- ... -- %>
      --
      -- Si se asigna false al atributo session la variable global session
      -- (usada más abajo) deja de estar disponible en el JSP.
      --
      -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      --%>

      <%-- -- Importación de clases propias de Java a este JSP, así como de clases -- creadas por uno mismo. --%>
        <%@ page import="java.util.List" import="java.util.Map" import="java.util.ArrayList"
          import="programacionweb.utilerias.UrlRelativo" %>


          <%-- -- Un JSP tiene acceso a varios **objetos globales** u **objetos implicitos**: -- -- * HttpServletRequest
            request -- * HttpServletResponse response -- * HttpSession session -- * ServletContext application -- *
            ServletConfig config --%>

            <%-- -- Declaración y asignación, así como acceso a los datos (atributos) -- enviados-compartidos por el
              Controlador haciendo uso del objeto *request*. -- -- El objeto *request* es la misma instancia obtenida
              por el Controlador en -- el parámetro "HttpServletRequest solicitud" del método doGet(). --%>

              <%-- -- Declaración e inicialización de otras **variables globales** --%>
                <% String htmlTitle="Cifrado Atbash" ; String mainCss=UrlRelativo.css(request, "/fondo2.css" ); %>

                <!DOCTYPE html>
                <html>
                <head>
                  <meta charset="utf-8">
                  <meta name="viewport" content="width=device-width, initial-scale=1">
                  <title><%= htmlTitle %></title>
                  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
                  <link rel="stylesheet" href="<%= mainCss %>">
                </head>
                <body>
                  <form style="margin: 5em 10em;" method="POST" action="">

                    <div class="field">
                      <div class="control">
                        <h1 class="title is-1 mr-auto ml-auto"> Cifrado Atbash </h1>
                      </div>
                    </div>

                    <div class="field">
                      <label class="label">Ingrese el texto</label>
                      <div class="control">
                        <textarea class="textarea is-primary" placeholder="Texto que desea cifrar" name="EntradaTexto"></textarea>
                      </div>
                    </div>

                    <div class="field">
                      <label class="label">Ingrese el alfabeto</label>
                      <div class="control">
                          <input class="input" type="text" placeholder="Alfabeto" name="EntradaTextoAlfabeto">
                        <!-- <textarea class="textarea is-primary" placeholder="Alfabeto" name="EntradaTextoAlfabeto"></textarea> -->
                      </div>
                    </div>

                    <div class="field">
                      <label class="label">Resultado</label>
                      <div class="control">
                    <textarea class="textarea pt-5 is-focused has-fixed-size is-danger" name="resultado" placeholder="Resultado" readonly>
                      ${resultado}
                    </textarea>
                    </div>
                    </div>

                    <div class="field">
                      <label class="label">Alfabeto ingresado</label>
                      <div class="control">
                    <textarea class="textarea pt-5 is-focused has-fixed-size" name="resultado" placeholder="Resultado" readonly>
                      ${alfabeto}
                    </textarea>
                    </div>
                    </div>


                <div class="field is-grouped">
                  <div class="control">
                    <button class="button is-link is-success" type="submit" value="cifrarata" name="accion">Cifrar</button>
                  </div>
                  <div class="control">
                    <button class="button is-link is-danger" type="submit" value="DEScifrarata" name="accion">Descifrar</button>
                  </div>
                  <div class="control">
                    <button class="button is-link is-info" type="submit" value="Otroata" name="accion">Cifrado Transposición por Grupos</button>
                  </div>
                </div>
                </form>
                    </div>
                  </div>
                  </section>
                </body>
                </html>
