package programacionweb.controladores;


import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.modelos.Cifrado;
import programacionweb.modelos.CodificarOtro;

@WebServlet("/inicio")
public class ControladorInicio extends HttpServlet
{
  public  String Permutacion,men,enc,alfabeto;
  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException{

        rep(solicitud, respuesta);
  }
  protected void doPost(HttpServletRequest solicitud, HttpServletResponse respuesta)
  throws ServletException, IOException{
   String resultado="";
    if(solicitud.getParameter("accion")!=null)
    {


      men=solicitud.getParameter("EntradaTexto");
      enc=solicitud.getParameter("EntradaTextoPermu");
      Permutacion=solicitud.getParameter("EntradaTextoPermu");
      alfabeto=solicitud.getParameter("EntradaTextoAlfabeto");


      if(solicitud.getParameter("accion").equalsIgnoreCase("DEScifrarata"))
      {
        resultado=CodificarOtro.codificar(men,alfabeto);
        solicitud.setAttribute("resultado", resultado);
      solicitud.setAttribute("alfabeto", alfabeto);
      rep2(solicitud, respuesta);
      }
      else if(solicitud.getParameter("accion").equalsIgnoreCase("cifrarata"))
      {
        resultado=CodificarOtro.codificar(men,alfabeto);
        solicitud.setAttribute("resultado", resultado);
      solicitud.setAttribute("alfabeto", alfabeto);
      rep2(solicitud, respuesta);
      }else


      if(solicitud.getParameter("accion").equalsIgnoreCase("Otroata"))
      {
        rep(solicitud, respuesta);
      }else

      if(solicitud.getParameter("accion").equalsIgnoreCase("Otro"))
      {
        rep2(solicitud, respuesta);
      }else


      if(solicitud.getParameter("accion").equalsIgnoreCase("cifrar"))
      {
        resultado=Cifrado.Cifrar(men, enc);
        solicitud.setAttribute("resultado", resultado);
      solicitud.setAttribute("Permutacion", Permutacion);
      rep(solicitud, respuesta);
      }else

      if(solicitud.getParameter("accion").equalsIgnoreCase("descifrar")){
        resultado=Cifrado.DESCifrar(men, enc);
                solicitud.setAttribute("resultado", resultado);
      solicitud.setAttribute("Permutacion", Permutacion);
      rep(solicitud, respuesta);
      }


    }

}

  protected void rep(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }

      protected void rep2(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaAtash.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }
}
