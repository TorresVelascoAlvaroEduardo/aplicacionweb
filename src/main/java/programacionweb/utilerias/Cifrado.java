package programacionweb.utilerias;

public final class Cifrado {

  public static String inicio(String texto, String permutacion, String tipoCifrado){
    String texto1 = texto.replace(" ", "");
    if(verificaPermutacion(permutacion)){
         if(tipoCifrado.equals("Cifrado")){
           String mensajeCifrado = cifrado(texto1, permutacion);
           return mensajeCifrado;
         }else{
           String mensajeDescifrado = descifrado(texto1, permutacion);
           return mensajeDescifrado;
         }
    } else
        return "Error: Permutacion no valida !" ;
  }

  public static boolean verificaPermutacion(String permutacion){
      boolean validacion = true;
      for(int i=1; i<=permutacion.length(); i++){
          if(permutacion.indexOf((char)(i +'0')) == -1)
              validacion = false;
      }
      return validacion;
  }

  public static String cifrado(String texto, String permutacion){
      int periodo = permutacion.length();
      String mensajeCifrado = "";
      int faltantes = texto.length() % periodo;
      for(int i=faltantes; i<=periodo; i++)
          texto += " ";

      for(int i=0; i<texto.length() - periodo; i+=periodo){
          char[] grupo = texto.substring(i, i + periodo).toCharArray();

          char[] grupoNuevo = new char[periodo];
          for(int j=0; j<periodo; j++){
              int index = Character.getNumericValue(permutacion.charAt(j) - 1);
              grupoNuevo[j] = grupo[index];
          }
          mensajeCifrado += String.valueOf(grupoNuevo);
      }
      mensajeCifrado = cadenaFinal(mensajeCifrado, periodo);
      return mensajeCifrado;
  }

  public static String descifrado(String textoCifrado, String permutacion){
      int periodo = permutacion.length();
      String mensajeDescifrado = "";
      for(int i=0; i<textoCifrado.length(); i+=periodo){
          char[] grupo = textoCifrado.substring(i, i + periodo).toCharArray();
          char[] grupoNuevo = new char[periodo];
          for(int j=0; j<periodo; j++){
              int index = Character.getNumericValue(permutacion.charAt(j) - 1);
              grupoNuevo[index] = grupo[j];
          }
          mensajeDescifrado += String.valueOf(grupoNuevo);
      }
      return mensajeDescifrado;
  }

  public static String cadenaFinal(String cadena, int periodo){
  int multiplo= periodo;
  String input = cadena;
   StringBuilder result = new StringBuilder();
  for (int i = 0; i < input.length(); i++) {
   if ((validamult(i, multiplo))) {
      result.append(" ");
    }
   result.append(input.charAt(i));
  }

  return result.toString();
}

public static boolean validamult(int numero, int multi){
    if (numero%multi==0)
  return true;
else
  return false;
}
}
