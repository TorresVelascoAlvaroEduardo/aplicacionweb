package programacionweb.modelos;

public class Cifrado {

    public static String Cifrar(String texto, String permutacion) {
      String texto1 = texto.replace("\n", "");
      String permutacion1 = permutacion.replace("\n", "");
      String texto2 = texto1.replace(" ", "");
      String permutacion2 = permutacion1.replace(" ", "");
        String mensaje = "";
        if(texto.length()>=1000){
            mensaje="Cadena mayor 1000 ingrese una mas corta";
        }else
        if (verificaPermutacion(permutacion2)) {
            mensaje = cifrado(texto2, permutacion2);
            System.out.println("Mensaje cifrado:" + mensaje);

        } else {
            System.out.println("Error: Permutacion no valida !");
            mensaje = "No se puede Cifrar porque la permutacion no es validad !";
        }
        return mensaje;
    }

    public static String DESCifrar(String texto, String permutacion) {
      String texto1 = texto.replace("\n", "");
      String permutacion1 = permutacion.replace("\n", "");
      String texto2 = texto1.replace(" ", "");
      String permutacion2 = permutacion1.replace(" ", "");
        String mensaje = "";
        if(texto.length()>=1000){
            mensaje="Cadena mayor 1000 ingrese una mas corta";
        }else
        if (verificaPermutacion(permutacion2)) {
            mensaje = descifrado(texto2, permutacion2);
            System.out.println("Mensaje descifrado:" + mensaje);
        } else {
            System.out.println("Error: Permutacion no valida !");
            mensaje = "No se puede Cifrar porque la permutacion no es validad, recuerda que deb ser valores del 1 al 9 y concecutivos!";
        }
        return mensaje;
    }


    public static boolean verificaPermutacion(String permutacion) {
        boolean validacion = true;
        for (int i = 1; i <= permutacion.length(); i++) {
            if (permutacion.indexOf((char) (i + '0')) == -1) {
                validacion = false;
            }
        }
        return validacion;
    }

    public static String cifrado(String texto, String permutacion) {
        int periodo = permutacion.length();
        String mensajeCifrado = "";
        int faltantes = texto.length() % periodo;
        for (int i = faltantes; i <= periodo; i++) {
            texto += " ";
        }
        for (int i = 0; i < texto.length() - periodo; i += periodo) {
            char[] grupo = texto.substring(i, i + periodo).toCharArray();
            char[] grupoNuevo = new char[periodo];
            for (int j = 0; j < periodo; j++) {
                int index = Character.getNumericValue(permutacion.charAt(j) - 1);
                grupoNuevo[j] = grupo[index];
            }
            mensajeCifrado += String.valueOf(grupoNuevo);
        }
        mensajeCifrado = cadenaFinal(mensajeCifrado, periodo);
        return mensajeCifrado;
    }

    public static String descifrado(String textoCifrado, String permutacion) {
        int periodo = permutacion.length();
        String mensajeDescifrado = "";
        for (int i = 0; i < textoCifrado.length(); i += periodo) {
            char[] grupo = textoCifrado.substring(i, i + periodo).toCharArray();
            char[] grupoNuevo = new char[periodo];
            for (int j = 0; j < periodo; j++) {
                int index = Character.getNumericValue(permutacion.charAt(j) - 1);
                grupoNuevo[index] = grupo[j];
            }
            mensajeDescifrado += String.valueOf(grupoNuevo);
        }
        return mensajeDescifrado;
    }

    public static String cadenaFinal(String cadena, int periodo){
    int multiplo= periodo;
    String input = cadena;
     StringBuilder result = new StringBuilder();
    for (int i = 0; i < input.length(); i++) {
     if ((validamult(i, multiplo))) {
        result.append(" ");
      }
     result.append(input.charAt(i));
    }

    return result.toString();
  }

  public static boolean validamult(int numero, int multi){
      if (numero%multi==0)
    return true;
  else
    return false;
  }

}
